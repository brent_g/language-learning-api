const mongoose = require("mongoose");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const config = require("config");

const schema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 4,
    maxlength: 255,
    required: true
  },
  email: {
    type: String,
    minlength: 5,
    maxlength: 255,
    required: true
  },
  password: {
    type: String,
    minlength: 5,
    maxlength: 1024,
    required: true
  },
  updated: { type: Date, default: Date.now }
});

schema.methods.generateAuthToken = () => {
  return jwt.sign({ _id: this._id }, config.get("jwtPrivateKey"));
};

const User = mongoose.model("User", schema);

validate = user => {
  const schema = {
    name: joi
      .string()
      .required()
      .min(4)
      .max(255),
    email: joi
      .string()
      .email()
      .required()
      .min(4)
      .max(255),
    password: joi.password.required()
  };

  return joi.validate(user, schema);
};

module.exports.User = User;
module.exports.validate = validate;
module.exports.schema = schema;
