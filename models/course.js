const mongoose = require("mongoose");
const Joi = require("joi");
const slugify = require("slugify");

const courseSchema = new mongoose.Schema({
  name: { type: String, required: true },
  slug: { type: String },
  words: {
    type: Array,
    required: true,
    validate: {
      isAsync: true,
      validator: function(value, callback) {
        const result = value && value.length > 0;
        callback(result);
      },
      message: "A course should contain atleast one word."
    }
  }
});

// create a course model
const Course = mongoose.model("Course", courseSchema);

validateId = id => {
  const schema = {
    _id: Joi.ObjectId().error(err => "Invalid ID format provided.")
  };

  return Joi.validate({ _id: id }, schema);
};

validate = course => {
  // schema to validate against
  const schema = Joi.object().keys({
    name: Joi.string()
      .min(3)
      .required(),
    words: Joi.array().required()
  });

  return Joi.validate(course, schema);
};

async function createCourse(data) {
  const course = new Course({
    name: data.name,
    slug: slugify(data.name, "-"),
    words: data.words
  });

  try {
    const result = await course.save();
    console.log("resultasdf", result);
    return result;
  } catch (error) {
    console.log(error.message);
  }
}

exports.courseSchema = courseSchema;
exports.Course = Course;
exports.validate = validate;
exports.validateId = validateId;
exports.createCourse = createCourse;
