const Joi = require("joi");
module.exports = () => {
  const JPC = require("joi-password-complexity");
  Joi.ObjectId = require("joi-objectid")(Joi);
  Joi.password = new JPC({
    min: 5,
    max: 26
  });
};
