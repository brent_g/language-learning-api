const express = require("express");
const cors = require("cors");

const home = require("../routes/home");
const courses = require("../routes/courses");
const users = require("../routes/users");

module.exports = app => {
  // middleware
  app.use(express.json());
  app.use(cors());

  // routes
  app.use("/api/users", users);
  app.use("/api/courses", courses);
  app.use("/", home);
};
