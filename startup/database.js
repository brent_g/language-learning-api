const mongoose = require("mongoose");

module.exports = () => {
  // connect to our mongodb server
  try {
    // apparently dont need to await this because mongoose handles connection buffering internally
    mongoose.connect("mongodb://localhost/llearning", {
      useNewUrlParser: true
    });
  } catch (ex) {
    console.log("Error connecting to mongoose", ex.message);
  }
};
