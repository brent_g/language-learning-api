const express = require("express");
const router = express.Router();
const _ = require("lodash");
const bcrypt = require("bcrypt");

const { User, validate } = require("../models/user");

router.get("/me", async (req, res) => {
  const user = User.findById(req.user._id).select("-password");
  res.send(user);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // check if the user isnt already registered
  const userExists = await User.findOne({ email: req.body.email });
  if (userExists) return res.status(400).send("User already registered.");

  // filter the req body and select these fields (prevents the user from sending additional malicious data)
  const user = new User(_.pick(req.body, ["name", "email", "password"]));

  // generate a salt using 10 interations
  const salt = await bcrypt.genSalt(10);

  // hash the user password and replace the password value on the user object
  user.password = await bcrypt.hash(user.password, salt);

  // save the user
  await user.save();

  // generate a jwt to send back to newly registered client
  const token = user.generateAuthToken();

  // add the jwt to the response headers so the user will be logged in and redirected after registering
  res
    .header("x-auth-token", token)
    .send(_.pick(user, ["_id", "name", "email"]));
});

module.exports = router;
