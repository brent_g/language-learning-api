const express = require("express");
const router = express.Router();

const {
  Course,
  createCourse,
  validate,
  validateId
} = require("../models/course");

router.get("/", async (req, res) => {
  const courses = await Course.find();
  res.send(courses);
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  const { error } = validateId(id);
  if (error) return res.status(400).send(error.details[0].message);

  const course = await Course.findById(id);
  if (!course)
    return res.status(404).send("the course with the given id was not found");

  res.send(course);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // add new course to the database
  const result = await createCourse(req.body);

  // return the course + id
  res.send(result);
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const course = await Course.findById(id);

  if (!course)
    return res.status(404).send("The course with the given id does not exist.");

  // validate the request body format is correct
  const { error } = validate(req.body);

  if (error) return res.status(400).send(error.details[0].message);

  // update the couse in the database
  const result = await Course.findOneAndUpdate({ _id: id }, req.body);

  // return the updated course object
  res.send(result);
});

//Delete
router.delete("/:id", async (req, res) => {
  // check if the course exists
  const { id } = req.params;
  const course = await Course.findById(id);
  if (!course) return res.status(400).send("Invalid request.");
  // remove the course from the database
  const result = await Course.findOneAndDelete({ _id: id });
  // and return the value to the client
  res.send(result);
});

module.exports = router;
