const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.send("Home page : GET");
});

router.get("/:id", (req, res) => {
  res.send("Home page : GET");
});

router.post("/", (req, res) => {
  res.send("Home page : POST");
});

router.put("/", (req, res) => {
  res.send("Home page : PUT");
});

router.delete("/", (req, res) => {
  res.send("Home page : DELETE");
});

module.exports = router;
